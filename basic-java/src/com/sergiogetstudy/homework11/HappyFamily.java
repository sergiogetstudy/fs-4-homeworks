package com.sergiogetstudy.homework11;

import com.sergiogetstudy.homework11.controller.FamilyController;
import com.sergiogetstudy.homework11.dao.CollectionFamilyDao;
import com.sergiogetstudy.homework11.dao.Dao;
import com.sergiogetstudy.homework11.domain.enums.DayOfWeek;
import com.sergiogetstudy.homework11.domain.family.Family;
import com.sergiogetstudy.homework11.domain.human.Human;
import com.sergiogetstudy.homework11.domain.human.man.Man;
import com.sergiogetstudy.homework11.domain.human.woman.Woman;
import com.sergiogetstudy.homework11.domain.pet.Pet;
import com.sergiogetstudy.homework11.domain.pet.dog.Dog;
import com.sergiogetstudy.homework11.domain.pet.domestic_cat.DomesticCat;
import com.sergiogetstudy.homework11.service.FamilyService;

import java.util.*;

public class HappyFamily {
    static {
        System.out.printf("%s class is loading...%n", HappyFamily.class.getSimpleName());
    }
    public static void main(String[] args) {
        Dao<Family> collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController();

        familyService.setFamilyDao(collectionFamilyDao);
        familyController.setFamilyService(familyService);

        familyController.printMenu();
    }
}
