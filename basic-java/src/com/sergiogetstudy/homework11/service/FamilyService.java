package com.sergiogetstudy.homework11.service;

import com.sergiogetstudy.homework11.dao.Dao;
import com.sergiogetstudy.homework11.domain.family.Family;
import com.sergiogetstudy.homework11.domain.human.Human;
import com.sergiogetstudy.homework11.domain.human.woman.Woman;
import com.sergiogetstudy.homework11.domain.pet.Pet;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class FamilyService {
    private Dao<Family> familyDao;

    public void setFamilyDao(Dao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllEntities();
    }
    public String displayAllFamilies() {
        String result = Stream
            .iterate(0, i -> i + 1)
            .limit(familyDao.getAllEntities().size())
            .map(String::valueOf)
            .reduce("", (acc, i) -> acc + (Integer.parseInt(i) + 1) + " - " + familyDao.getAllEntities().get(Integer.parseInt(i)).prettyFormat() + "\n");
        System.out.println(result);

        return result;
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        return familyDao.getAllEntities()
                .stream()
                .filter(family -> family.countFamily() > count)
                .peek((family) -> {
                    System.out.println(family.prettyFormat());
                })
                .toList();
    }
    public List<Family>  getFamiliesLessThan(int count) {
        return familyDao.getAllEntities()
                .stream()
                .filter(family -> family.countFamily() < count)
                .peek((family) -> System.out.println(family.prettyFormat()))
                .toList();
    }

    public int countFamiliesWithMemberNumber(int memberNumber) {
        return (int) familyDao.getAllEntities()
                .stream()
                .filter(family -> family.countFamily() == memberNumber)
                .count();
    }
    public Family createNewFamily(Human woman, Human man) {
        if(woman == null || man == null) return null;
        Family family = new Family(woman, man);
        familyDao.saveEntity(family);
        return family;
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteEntity(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        if(family == null) return null;
        Human child = ((Woman) family.getMother()).bornChild(childMansName, childWomansName);
        family.addChild(child);
        if (familyDao.getAllEntities().contains(family)) familyDao.saveEntity(family);

        return family;
    }
    public Family adoptChild(Family family, Human child) {
        if(family == null) return null;

        family.addChild(child);
        if (familyDao.getAllEntities().contains(family)) familyDao.saveEntity(family);

        return family;
    }
    public boolean deleteAllChildrenOlderThan(int childAge) {

        boolean isToEdit = familyDao.getAllEntities()
                .stream()
                .anyMatch(family -> family.getChildren()
                        .stream()
                        .anyMatch(child -> child.getBirthDate() < LocalDate.now().minusYears(childAge).toEpochDay())
                );

        if(isToEdit) {
            familyDao.getAllEntities()
                .forEach(family -> {
                    family.setChildren(family.getChildren()
                            .stream()
                            .filter(child -> child.getBirthDate() > LocalDate.now().minusYears(childAge).toEpochDay())
                            .toList());
                    familyDao.saveEntity(family);
                });
        }

        return isToEdit;
    }
    public int count() {
        return familyDao.getAllEntities().size();
    }
    public Family getFamilyById(int index) {
        return familyDao.getEntityByIndex(index);
    }
    public Set<Pet> getPets(int index) {
        if(familyDao.getEntityByIndex(index) == null) return null;
        return familyDao.getEntityByIndex(index).getPets();
    }
    public boolean addPet(int index, Pet pet) {
        if(pet == null) return false;

        Family family = familyDao.getEntityByIndex(index);

        if(family.getPets() == null) {
            family.setPets(new HashSet<>());
        };

        return family.getPets().add(pet);
    }
}
