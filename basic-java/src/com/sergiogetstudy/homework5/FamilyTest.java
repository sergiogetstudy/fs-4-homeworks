package com.sergiogetstudy.homework5;

import org.junit.jupiter.api.*;

class FamilyTest {
    Family simpleFamily;
    Human childNum1;
    Human childNum2;
    Human childNum3;
    Pet simplePet;
    @BeforeAll
    static void beforeAll() { System.out.println("Testing Family class..."); }
    @BeforeEach
    void setUp() {
        simpleFamily = new Family(
                new Human("", "", (short) 0),
                new Human("", "", (short) 0)
        );
        childNum1 = new Human("", "", (short) 1);
        childNum2 = new Human("", "", (short) 2);
        childNum3 = new Human("", "", (short) 3);
        simplePet = new Pet();
    }
    @Test
    void addChild() {
        Human child = new Human("", "", (short) 1);

        Assertions.assertEquals(simpleFamily.getChildren().length, 0);

        simpleFamily.addChild(child);

        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[] {child});

        Assertions.assertEquals(simpleFamily.getChildren().length, 1);
    }

    @Test
    void deleteChild() {
        Assertions.assertFalse(simpleFamily.deleteChild(-1));
        Assertions.assertFalse(simpleFamily.deleteChild(0));
        Assertions.assertFalse(simpleFamily.deleteChild(100));

        simpleFamily.addChild(childNum1);
        simpleFamily.addChild(childNum2);
        simpleFamily.addChild(childNum3);

        simpleFamily.deleteChild(1);

        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[]{childNum1, childNum3});

        simpleFamily.addChild(childNum2);
        Assertions.assertFalse(simpleFamily.deleteChild(3));
        simpleFamily.deleteChild(0);

        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[]{childNum3, childNum2});
    }

    @Test
    void testDeleteChild() {
        simpleFamily.deleteChild(childNum3);
        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[]{});

        simpleFamily.addChild(childNum1);
        simpleFamily.addChild(childNum2);
        simpleFamily.deleteChild(childNum3);

        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[]{childNum1, childNum2});

        Assertions.assertTrue(simpleFamily.deleteChild(new Human("", "", (short) 1)));

        Assertions.assertArrayEquals(simpleFamily.getChildren(), new Human[]{childNum2});
    }

    @Test
    void countFamily() {
        Assertions.assertEquals(simpleFamily.countFamily(), 2);

        simpleFamily.addChild(childNum1);

        Assertions.assertEquals(simpleFamily.countFamily(), 3);
    }

    @Test
    void testToString() {
        Assertions.assertEquals(
                simpleFamily.toString(),
                "Family{" +
                        "mother=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "father=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "children=[], pet=null}"
        );

        simpleFamily.setPet(simplePet);
        Assertions.assertEquals(
                simpleFamily.toString(),
                "Family{" +
                        "mother=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "father=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "children=[], " +
                        "pet=null{nickname='null', age=0, trickLevel=not very cunning, habits=null}}"
        );
    }

    @AfterEach
    void tearDown() {
        simpleFamily = null;
        childNum1 = null;
        childNum2 = null;
        childNum3 = null;
        simplePet = null;
    }

    @AfterAll
    static void afterAll() { System.out.println("Finish testing Family class!"); }
}