package com.sergiogetstudy.homework5;

public enum Species {
    CAT("cat", false, 4, true),
    DOG("dog", false, 4, true),
    RACCOON("raccoon", false, 4, true);

    private final String petSpecies;
    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    Species(String petSpecies, boolean canFly, int numberOfLegs, boolean hasFur) {
        this.petSpecies = petSpecies;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public String getPetSpecies() {
        return petSpecies;
    }

    @Override
    public String toString() {
        return "Species{" +
                "species='" + petSpecies + '\'' +
                ", can fly=" + canFly +
                ", number of legs=" + numberOfLegs +
                ", has fur=" + hasFur +
                '}';
    }
}
