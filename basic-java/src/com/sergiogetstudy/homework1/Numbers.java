package com.sergiogetstudy.homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    static final String[][] QUESTION_MATRIX = {
            {"1939", "When did the World War II begin?"},
            {"1981", "What year is recognized as the year the Tim Berners-Lee invented the World Wide Web?"},
            {"1939", "When did the Soviet Union collapse?"},
            {"1789", "When was French Revolution formed?"},
    };
    static final int ANSWER_POSITION = 0;
    static final int QUESTION_POSITION = 1;

    public static void main(String[] args) {
        //get random question
        int randomNum = new Random().nextInt(QUESTION_MATRIX.length);
        String randomQuestion = QUESTION_MATRIX[randomNum][QUESTION_POSITION];
        int guessNumber = Integer.parseInt(QUESTION_MATRIX[randomNum][ANSWER_POSITION]);

        //game beginning
        var scanner = new Scanner(System.in);
        System.out.print("Please enter your name: ");
        String name = scanner.nextLine();
        System.out.println("Let the game begin!");
        System.out.println(randomQuestion);

        //init log array
        int[] logArray = new int[10];

        for (int i = 0; ; i++) {
            //get input from user
            System.out.print("Enter the number: ");
            while (!scanner.hasNextInt()) {
                System.out.print("Try enter the number again: ");
                scanner.next();
            }
            int inputNum = scanner.nextInt();

            //check if the array needs to be expanded
            if(logArray.length <= i) {
                logArray = Arrays.copyOf(logArray, i + (i / 3 + 1));
            }
            //save log in array
            logArray[i] = inputNum;

            //check is match input and right answer value
            if (guessNumber > inputNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (guessNumber < inputNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s\n", name);
                //split log array to leave only used cells
                logArray = Arrays.copyOfRange(logArray, 0, i + 1);
                //output log
                Arrays.sort(logArray);
                System.out.print("Your numbers: ");
                for(int log : logArray) {
                    System.out.printf("%d ", log);
                }
                break;
            }
        }
    }
}
