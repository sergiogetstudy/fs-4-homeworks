package com.sergiogetstudy.homework9;

import com.sergiogetstudy.homework9.controller.FamilyController;
import com.sergiogetstudy.homework9.dao.CollectionFamilyDao;
import com.sergiogetstudy.homework9.domain.enums.DayOfWeek;
import com.sergiogetstudy.homework9.domain.family.Family;
import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.human.man.Man;
import com.sergiogetstudy.homework9.domain.human.woman.Woman;
import com.sergiogetstudy.homework9.domain.pet.Pet;
import com.sergiogetstudy.homework9.domain.pet.dog.Dog;
import com.sergiogetstudy.homework9.domain.pet.domestic_cat.DomesticCat;
import com.sergiogetstudy.homework9.service.FamilyService;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class HappyFamily {
    static {
        System.out.printf("%s class is loading...%n", HappyFamily.class.getSimpleName());
    }
    public static void main(String[] args) {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController();

        familyService.setFamilyDao(collectionFamilyDao);
        familyController.setFamilyService(familyService);

        Human charlie = new Man("Charlie", "Runkle", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        Human marcy = new Woman("Marcy", "Runkle", Human.getStamp("1/01/1984", "d/MM/yyyy"));

        familyController.createNewFamily(marcy, charlie);
        familyController.getFamilyById(0);
        familyController.addPet(0, new DomesticCat());

        Human andrew = new Man("Andrew", "Van der Beek", Human.getStamp("01/01/1953", "d/MM/yyyy"));
        Human jane = new Woman("Jane", "Van der Beek", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        new Family(jane, andrew);
        familyController.createNewFamily(jane, andrew);


        Human al = new Man("Al", "Moody", Human.getStamp("01/01/1950", "d/MM/yyyy"));
        Human margaret = new Woman("Margaret", "Moody", Human.getStamp("01/01/1951", "d/MM/yyyy"));
        Family moodiesOlder = familyController.createNewFamily(jane, andrew);


        Dog catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        Set<Pet> moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        Human karen = new Woman("Karen", "Van der Beek", Human.getStamp("01/01/1983", "d/MM/yyyy"), jane, andrew);
        Human hank = new Man("Hank", "Moody", Human.getStamp("01/01/1983", "d/MM/yyyy"), (byte) 27,
                                createSchedule(), moodiesOlder, moodiesPets, margaret, al);

        familyController.createNewFamily(karen, hank);
        familyController.addPet(2, catStevens);

        familyController.bornChild(familyController.getFamilyById(2), "Becca", "Levon");

        Human levon = new Man("Levon", "Moody", "1/12/1999", (byte) 100);

        familyController.adoptChild(familyController.getFamilyById(2), levon);

        familyController.displayAllFamilies();

        System.out.println("familyController.deleteAllChildrenOlderThen(1) = " +
                familyController.deleteAllChildrenOlderThen(1));

        familyController.displayAllFamilies();

        System.out.println("familyController.getAllFamilies() = " +
                familyController.getAllFamilies());

        System.out.println("familyController.getPets(2) = " +
                familyController.getPets(2));

        System.out.println("familyController.getFamiliesBiggerThan(2).size() = " +
                familyController.getFamiliesBiggerThan(2).size());

        System.out.println("familyController.getFamiliesLessThan(2).size() = " +
                familyController.getFamiliesLessThan(2).size());

        System.out.println("familyController.countFamiliesWithMemberNumber() = " +
                familyController.countFamiliesWithMemberNumber(2));

        System.out.println("familyController.deleteFamilyByIndex(2) = " +
                familyController.deleteFamilyByIndex(2));

        familyController.displayAllFamilies();

        System.out.println("familyController.count() = " +
                familyController.count());


//        for (int i = 0; i < 10_000_000; i++) {
//            new Human("Agent", "Smith", (short) 1111 );
//        }
    }
    public static Map<String, String> createSchedule() {
        Map<String, String> schedule = new HashMap<>();

        for (DayOfWeek day: DayOfWeek.values()) {
            schedule.put(day.toString(), "");
        }

        return schedule;
    }
}
