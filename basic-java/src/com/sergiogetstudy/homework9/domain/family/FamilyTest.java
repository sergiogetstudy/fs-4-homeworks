package com.sergiogetstudy.homework9.domain.family;

import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.human.man.Man;
import com.sergiogetstudy.homework9.domain.human.woman.Woman;
import com.sergiogetstudy.homework9.domain.pet.Pet;
import com.sergiogetstudy.homework9.domain.pet.domestic_cat.DomesticCat;
import org.junit.jupiter.api.*;

import java.util.*;

class FamilyTest {
    Family simpleFamily;
    Human childNum1;
    Human childNum2;
    Human childNum3;
    Set<Pet> simplePets;
    @BeforeAll
    static void beforeAll() { System.out.println("Testing Family class..."); }
    @BeforeEach
    void setUp() {
        simpleFamily = new Family(
                new Woman("", "", (short) 0),
                new Man("", "", (short) 0)
        );
        childNum1 = new Man("", "", (short) 1);
        childNum2 = new Man("", "", (short) 2);
        childNum3 = new Man("", "", (short) 3);
        simplePets = new HashSet<>();
        simplePets.add(new DomesticCat());
    }
    @Test
    void addChild() {
        Human child = new Man("", "", (short) 1);
        List<Human> testChildren = new ArrayList<>();
        testChildren.add(child);

        Assertions.assertEquals(simpleFamily.getChildren().size(), 0);

        simpleFamily.addChild(child);

        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), testChildren.toArray());

        Assertions.assertEquals(simpleFamily.getChildren().size(), 1);
    }

    @Test
    void deleteChild() {
        Assertions.assertFalse(simpleFamily.deleteChild(-1));
        Assertions.assertFalse(simpleFamily.deleteChild(0));
        Assertions.assertFalse(simpleFamily.deleteChild(100));

        simpleFamily.addChild(childNum1);
        simpleFamily.addChild(childNum2);
        simpleFamily.addChild(childNum3);

        simpleFamily.deleteChild(1);

        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), Arrays.asList(new Human[]{childNum1, childNum3}).toArray());

        simpleFamily.addChild(childNum2);
        Assertions.assertFalse(simpleFamily.deleteChild(3));
        simpleFamily.deleteChild(0);

        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), Arrays.asList(new Human[]{childNum3, childNum2}).toArray());
    }

    @Test
    void testDeleteChild() {
        simpleFamily.deleteChild(childNum3);
        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), new ArrayList<>().toArray());

        simpleFamily.addChild(childNum1);
        simpleFamily.addChild(childNum2);
        simpleFamily.deleteChild(childNum3);

        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), Arrays.asList(new Human[]{childNum1, childNum2}).toArray());

        Assertions.assertTrue(simpleFamily.deleteChild(new Man("", "", (short) 1)));

        Assertions.assertArrayEquals(simpleFamily.getChildren().toArray(), Arrays.asList(new Human[]{childNum2}).toArray());
    }

    @Test
    void countFamily() {
        Assertions.assertEquals(simpleFamily.countFamily(), 2);

        simpleFamily.addChild(childNum1);

        Assertions.assertEquals(simpleFamily.countFamily(), 3);
    }

    @Test
    void testToString() {
        Assertions.assertEquals(
                simpleFamily.toString(),
                "Family{" +
                        "mother=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "father=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "children=[], pets=null}"
        );

        simpleFamily.setPets(simplePets);
        Assertions.assertEquals(
                simpleFamily.toString(),
                "Family{" +
                        "mother=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "father=Human{name='', surname='', year=0, iq=0, father=null, mother=null}, " +
                        "children=[], " +
                        "pets=[Species{species='cat', can fly=false, number of legs=4, has fur=true}" +
                        "{nickname='null', age=0, trickLevel=not very cunning, habits=null}]}"
        );
    }

    @AfterEach
    void tearDown() {
        simpleFamily = null;
        childNum1 = null;
        childNum2 = null;
        childNum3 = null;
        simplePets = null;
    }

    @AfterAll
    static void afterAll() { System.out.println("Finish testing Family class!"); }
}