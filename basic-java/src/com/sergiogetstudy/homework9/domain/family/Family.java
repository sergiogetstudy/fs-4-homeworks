package com.sergiogetstudy.homework9.domain.family;

import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.util.*;

public class Family {
    {
        System.out.printf("%s instance is loading...%n", this.getClass().getSimpleName());
    }
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    static {
        System.out.printf("%s class is loading...%n", Family.class.getSimpleName());
    }
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.mother.setSurname(this.father.getSurname());
    }

    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public List<Human> getChildren() {
        return children;
    }
    public void setChildren(List<Human> children) {
        this.children = children;
    }
    public Set<Pet> getPets() {
        return pets;
    }
    public void setPets(Set<Pet> pets) {
        this.pets = pets;
        for (Human child: getChildren()) {
            child.setPets(pets);
        }
        mother.setPets(pets);
        father.setPets(pets);
    }

    public void addChild(Human child) {
        List<Human> children = new ArrayList<>(getChildren());

        child.setMother(getMother());
        child.setFather(getFather());
        child.setPets(getPets());
        children.add(child);

        setChildren(children);
    }
    public boolean deleteChild(int index) {
        List<Human> children = getChildren();

        if(index < 0 || index > children.size() - 1) return false;

        List<Human> resultArr = new ArrayList<>(getChildren());
        resultArr.remove(index);

        setChildren(resultArr);
        return true;
    }
    public boolean deleteChild(Human human) {
        List<Human> children = new ArrayList<>(getChildren());
        boolean isSucces = children.remove(human);
        if(isSucces) setChildren(children);

        return isSucces;
    }
    public int countFamily() {
        return 2 + getChildren().size();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + getMother() +
                ", father=" + getFather() +
                ", children=" + Arrays.toString(getChildren().toArray()) +
                ", pets=" + (getPets() == null ? null : getPets().toString()) +
                '}';
    }
    public void greetPet(Pet pet) {
        System.out.printf("Hello, %s", pet.getNickname());
    }
    public void describePet(Pet pet) {
        System.out.printf("В мене є %s, йому %s років, він %s ",
                pet.getSpecies(), pet.getAge(), pet.getTrickLevel());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize - " + this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return getMother().equals(family.getMother()) && getFather().equals(family.getFather());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMother(), getFather());
    }
}
