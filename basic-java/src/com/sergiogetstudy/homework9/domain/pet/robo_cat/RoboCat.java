package com.sergiogetstudy.homework9.domain.pet.robo_cat;

import com.sergiogetstudy.homework9.domain.enums.Species;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.util.Set;

public class RoboCat extends Pet {
    Species species = Species.ROBO_CAT;
    public RoboCat() {}

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Hello, host. I'm - %s. Can I help you?%n", getNickname());
    }
}
