package com.sergiogetstudy.homework9.domain.pet.fish;

import com.sergiogetstudy.homework9.domain.enums.Species;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.util.Set;

public class Fish extends Pet {
    final Species species = Species.FISH;

    public Fish() {}
    public Fish(String nickname) {
        super(nickname);
    }
    public Fish(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Bul bul");
    }
}
