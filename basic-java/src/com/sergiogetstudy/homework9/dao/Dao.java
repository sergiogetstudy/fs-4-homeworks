package com.sergiogetstudy.homework9.dao;

import com.sergiogetstudy.homework9.domain.family.Family;

import java.util.List;

public interface Dao<T> {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
}