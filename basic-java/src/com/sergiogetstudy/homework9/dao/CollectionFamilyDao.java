package com.sergiogetstudy.homework9.dao;

import com.sergiogetstudy.homework9.domain.family.Family;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionFamilyDao implements Dao {

    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return Collections.unmodifiableList(families);
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if(index < 0 || index >= families.size()) return null;
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if(index < 0 || index >= families.size()) return false;
        return families.remove(index) != null;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        if(families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
    }
}
