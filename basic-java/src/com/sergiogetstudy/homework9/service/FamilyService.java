package com.sergiogetstudy.homework9.service;

import com.sergiogetstudy.homework9.dao.Dao;
import com.sergiogetstudy.homework9.domain.family.Family;
import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.human.woman.Woman;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.time.LocalDate;
import java.util.*;

public class FamilyService {
    private Dao<Family> familyDao;

    public void setFamilyDao(Dao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    public String displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < families.size(); i++) {
            result.append(i).append(" - ").append(families.get(i)).append("\n");
        }
        System.out.println(result);

        return result.toString();
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> filteredList = new ArrayList<>();

        for(int i = 0; i < families.size(); i++) {
            Family family = families.get(i);

            if(family.countFamily() > count) {
                filteredList.add(family);
                System.out.println(i + " - " + family);
            }
        }

        return filteredList;
    }
    public List<Family>  getFamiliesLessThan(int count) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> filteredList = new ArrayList<>();

        for(int i = 0; i < families.size(); i++) {
            Family family = families.get(i);

            if(family.countFamily() < count) {
                filteredList.add(family);
                System.out.println(i + " - " + family);
            }
        }
        return filteredList;
    }

    public int countFamiliesWithMemberNumber(int memberNumber) {
        int count = 0;
        for (Family family: familyDao.getAllFamilies()) {
            if(family.countFamily() == memberNumber) count++;
        }
        return count;
    }
    public Family createNewFamily(Human woman, Human man) {
        if(woman == null || man == null) return null;
        Family family = new Family(woman, man);
        familyDao.saveFamily(family);
        return family;
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        if(family == null) return null;
        Human child = ((Woman) family.getMother()).bornChild(childMansName, childWomansName);
        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public Family adoptChild(Family family, Human child) {
        if(family == null) return null;

        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public boolean deleteAllChildrenOlderThen(int childAge) {
        List<Family> families = familyDao.getAllFamilies();
        boolean isChanged = false;
        for (Family family : families) {
            for (Human child : family.getChildren()) {
                System.out.println("child.getBirthDate() " + child.getBirthDate());
                System.out.println("LocalDate.now().minusYears(childAge).toEpochDay() " + LocalDate.now().minusYears(childAge).toEpochDay());
                if(child.getBirthDate() < LocalDate.now().minusYears(childAge).toEpochDay()) {
                    family.deleteChild(child);
                    familyDao.saveFamily(family);
                    isChanged = true;
                };
            }
        }
        return isChanged;
    }
    public int count() {
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index) {
        if(familyDao.getFamilyByIndex(index) == null) return null;
        return familyDao.getFamilyByIndex(index).getPets();
    }
    public boolean addPet(int index, Pet pet) {
        if(pet == null) return false;
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPets();
        if(pets == null) {
            family.setPets(new HashSet<>());
            pets = family.getPets();
        };
        boolean isChanged = pets.add(pet);
        family.setPets(pets);
        familyDao.saveFamily(family);

        return isChanged;
    }
}
