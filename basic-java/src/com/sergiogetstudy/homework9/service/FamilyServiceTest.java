package com.sergiogetstudy.homework9.service;

import com.sergiogetstudy.homework9.dao.CollectionFamilyDao;
import com.sergiogetstudy.homework9.domain.family.Family;
import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.human.man.Man;
import com.sergiogetstudy.homework9.domain.human.woman.Woman;
import com.sergiogetstudy.homework9.domain.pet.Pet;
import com.sergiogetstudy.homework9.domain.pet.dog.Dog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    Human charlie, marcy;
    Human andrew, jane;
    Human al, margaret;
    Human karen, hank;
    Pet catStevens;
    Set<Pet> moodiesPets;
    Family runkles;
    Family vanDerBeek;
    Family moodiesOlder;
    Family moodies;

    List<Family> families;
    CollectionFamilyDao collectionFamilyDao;
    FamilyService familyService;

    @BeforeEach
    void setUp() {
        charlie = new Man("Charlie", "Runkle", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        marcy = new Woman("Marcy", "Runkle", Human.getStamp("01/01/1984", "d/MM/yyyy"));
        runkles = new Family(marcy, charlie);

        andrew = new Man("Andrew", "Van der Beek", Human.getStamp("01/01/1953", "d/MM/yyyy"));
        jane = new Woman("Jane", "Van der Beek", Human.getStamp("01/01/1957", "d/MM/yyyy"));
        vanDerBeek= new Family(jane, andrew);

        al = new Man("Al", "Moody", Human.getStamp("01/01/1950", "d/MM/yyyy"));
        margaret = new Woman("Margaret", "Moody", Human.getStamp("01/01/1951", "d/MM/yyyy"));
        moodiesOlder = new Family(margaret, al);

        catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        karen = new Woman("Karen", "Van der Beek", Human.getStamp("01/01/1981", "d/MM/yyyy"), jane, andrew);
        hank = new Man("Hank", "Moody", Human.getStamp("01/01/1980", "d/MM/yyyy"), (byte) 27,
                new HashMap<>(), moodiesOlder, moodiesPets, margaret, al);
        moodies = new Family(karen, hank);

        families = new ArrayList<>();
        families.add(runkles);
        families.add(vanDerBeek);
        families.add(moodiesOlder);
        families.add(moodies);

        collectionFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService();
        familyService.setFamilyDao(collectionFamilyDao);
    }

    @AfterEach
    void tearDown() {
        charlie = null; marcy = null;
        andrew = null; jane = null;
        al = null; margaret = null;
        karen = null; hank = null;
        catStevens = null;
        moodiesPets = null;
        runkles = null;
        vanDerBeek = null;
        moodiesOlder = null;
        moodies = null;

        families = null;
        collectionFamilyDao = null;
        familyService = null;
    }

    @Test
    void getAllFamilies() {
        assertEquals(familyService.getAllFamilies(), new ArrayList<>());
        assertEquals(familyService.getAllFamilies(), collectionFamilyDao.getAllFamilies());
        Family moodies = familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getAllFamilies(), new ArrayList<>(List.of(moodies)));
    }

    @Test
        void displayAllFamilies() {
        assertEquals(familyService.displayAllFamilies(), "");
        familyService.createNewFamily(karen, hank);
        assertEquals(
                familyService.displayAllFamilies(),
                "0 - Family{" +
                        "mother=Human{name='Karen', surname='Moody', birthDate=1/01/1981, " +
                        "iq=0, father=Andrew Van der Beek, mother=Jane Van der Beek}, " +
                        "father=Human{name='Hank', surname='Moody', birthDate=1/01/1980, " +
                        "iq=27, father=Al Moody, mother=Margaret Moody}, " +
                        "children=[], pets=null}\n");
    }

    @Test
    void getFamiliesBiggerThan() {
        assertEquals(familyService.getFamiliesBiggerThan(0).size(), 0);
        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(familyService.getFamiliesBiggerThan(2).size(), 1);
        assertEquals(familyService.getFamiliesBiggerThan(-1).size(), 2);
    }

    @Test
    void getFamiliesLessThan() {
        assertEquals(familyService.getFamiliesLessThan(0).size(), 0);
        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(familyService.getFamiliesLessThan(4).size(), 2);
        assertEquals(familyService.getFamiliesLessThan(3).size(), 1);
        assertEquals(familyService.getFamiliesLessThan(-1).size(), 0);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        assertEquals(familyService.countFamiliesWithMemberNumber(0), 0);

        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);

        assertEquals(familyService.countFamiliesWithMemberNumber(2), 2);

        familyService.bornChild(moodies, "Levon", "Becca");

        assertEquals(familyService.countFamiliesWithMemberNumber(3), 1);
        assertEquals(familyService.countFamiliesWithMemberNumber(2), 1);
        assertEquals(familyService.countFamiliesWithMemberNumber(-1), 0);
    }

    @Test
    void createNewFamily() {
        assertEquals(familyService.createNewFamily(null, null), null);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodies = familyService.createNewFamily(karen, hank);
        assertEquals(moodies, new Family(karen, hank));
        assertEquals(familyService.getAllFamilies().size(), 1);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getAllFamilies().size(), 1);
        familyService.createNewFamily(marcy, charlie);
        assertEquals(familyService.getAllFamilies().size(), 2);
    }

    @Test
    void deleteFamilyByIndex() {
        assertFalse(familyService.deleteFamilyByIndex(-1));
        assertEquals(familyService.getAllFamilies().size(), 0);

        familyService.createNewFamily(karen, hank);
        Family runkles = familyService.createNewFamily(marcy, charlie);

        assertTrue(familyService.deleteFamilyByIndex(0));
        assertEquals(familyService.getAllFamilies().size(), 1);

        assertEquals(familyService.getFamilyById(0), runkles);
        assertFalse(familyService.deleteFamilyByIndex(1));
    }

    @Test
    void bornChild() {
        assertEquals(familyService.bornChild(null, null, null), null);
        Family moodies = new Family(karen, hank);
        assertEquals(moodies.countFamily(), 2);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(moodies.countFamily(), 3);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodiesInDao, "Levon", "Becca");
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().size(), 1);
    }

    @Test
    void adoptChild() {
        familyService.adoptChild(null, charlie);
        Family moodies = new Family(karen, hank);
        assertEquals(moodies.countFamily(), 2);
        familyService.adoptChild(moodies, charlie);
        assertEquals(moodies.countFamily(), 3);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.adoptChild(moodiesInDao, charlie);
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().get(0), charlie);
    }

    @Test
    void deleteAllChildrenOlderThen() {
        assertFalse(familyService.deleteAllChildrenOlderThen(-1));
        assertFalse(familyService.deleteAllChildrenOlderThen(0));
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.adoptChild(moodiesInDao, charlie);
        assertTrue(familyService.deleteAllChildrenOlderThen(0));
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().size(), 0);
    }

    @Test
    void count() {
        assertEquals(familyService.count(), 0);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.count(), 1);
        familyService.deleteFamilyByIndex(0);
        assertEquals(familyService.count(), 0);
    }

    @Test
    void getFamilyById() {
        assertSame(familyService.getFamilyById(0), null);
        collectionFamilyDao.saveFamily(runkles);
        assertSame(familyService.getFamilyById(0), collectionFamilyDao.getFamilyByIndex(0));
    }

    @Test
    void getPets() {
        assertEquals(familyService.getPets(0), null);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getPets(0), null);
        familyService.addPet(0, new Dog());
        assertEquals(familyService.getPets(0), collectionFamilyDao.getFamilyByIndex(0).getPets());
    }

    @Test
    void addPet() {
        assertEquals(familyService.addPet(0, null), false);
        familyService.createNewFamily(karen, hank);
        assertTrue(familyService.addPet(0, new Dog()));
        assertEquals(familyService.getPets(0).size(), 1);
    }
}