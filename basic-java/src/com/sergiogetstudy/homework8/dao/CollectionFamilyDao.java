package com.sergiogetstudy.homework8.dao;

import com.sergiogetstudy.homework8.domain.family.Family;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionFamilyDao implements Dao<Family> {

    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllEntities() {
        return Collections.unmodifiableList(families);
    }

    @Override
    public Family getEntityByIndex(int index) {
        if(index < 0 || index >= families.size()) return null;
        return families.get(index);
    }

    @Override
    public boolean deleteEntity(int index) {
        if(index < 0 || index >= families.size()) return false;
        return families.remove(index) != null;
    }

    @Override
    public boolean deleteEntity(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveEntity(Family family) {
        if(families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
    }
}
