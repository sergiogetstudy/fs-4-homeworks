package com.sergiogetstudy.homework8.dao;

import java.util.List;

public interface Dao<T> {
    List<T> getAllEntities();
    T getEntityByIndex(int index);
    boolean deleteEntity(int index);
    boolean deleteEntity(T family);
    void saveEntity(T family);
}
