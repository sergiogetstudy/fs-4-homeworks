package com.sergiogetstudy.homework8.domain.enums;

public enum Species {
    DOMESTIC_CAT("cat", false, 4, true),
    DOG("dog", false, 4, true),
    FISH("fish", false, 0, false),
    ROBO_CAT("robo cat", false, 4, false),
    UNKNOWN("unknown", false, 0, false);

    private final String petSpecies;
    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    Species(String petSpecies, boolean canFly, int numberOfLegs, boolean hasFur) {
        this.petSpecies = petSpecies;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public String getPetSpecies() {
        return petSpecies;
    }

    @Override
    public String toString() {
        return "Species{" +
                "species='" + petSpecies + '\'' +
                ", can fly=" + canFly +
                ", number of legs=" + numberOfLegs +
                ", has fur=" + hasFur +
                '}';
    }
}
