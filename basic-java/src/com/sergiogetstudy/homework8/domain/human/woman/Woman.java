package com.sergiogetstudy.homework8.domain.human.woman;

import com.sergiogetstudy.homework8.domain.family.Family;
import com.sergiogetstudy.homework8.domain.human.Human;
import com.sergiogetstudy.homework8.domain.human.man.Man;
import com.sergiogetstudy.homework8.domain.pet.Pet;
import com.sergiogetstudy.homework8.domain.enums.BoyName;
import com.sergiogetstudy.homework8.domain.enums.GirlName;
import com.sergiogetstudy.homework8.domain.interfaces.HumanCreator;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public final class Woman extends Human implements HumanCreator {

    public Woman(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, short year, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet) {
        super(name, surname, year, iq, schedule, family, pet);
    }

    public void makeup() {
        System.out.println("Let's makeup!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname() + ". How are you?");
    }

    @Override
    public Human bornChild(String childMansName, String childWomansName) {
        boolean isBoy = new Random().nextBoolean();

        if (isBoy) {
            return new Man(
                    childMansName,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    (short) 0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets()
            );
        } else {
            return new Woman(
                    childWomansName,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    (short) 0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets()
            );
        }
    }
}
