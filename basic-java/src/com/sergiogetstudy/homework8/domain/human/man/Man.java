package com.sergiogetstudy.homework8.domain.human.man;

import com.sergiogetstudy.homework8.domain.family.Family;
import com.sergiogetstudy.homework8.domain.human.Human;
import com.sergiogetstudy.homework8.domain.pet.Pet;

import java.util.Map;
import java.util.Set;

public final class Man extends Human {
    public Man(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, short year, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet) {
        super(name, surname, year, iq, schedule, family, pet);
    }

    public void repairCar() {
        System.out.println("Let's repair the car!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname());
    }
}
