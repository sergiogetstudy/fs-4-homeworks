package com.sergiogetstudy.homework8.domain.pet.robo_cat;

import com.sergiogetstudy.homework8.domain.pet.Pet;
import com.sergiogetstudy.homework8.domain.enums.Species;

import java.util.Set;

public class RoboCat extends Pet {
    Species species = Species.ROBO_CAT;
    public RoboCat() {}

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Hello, host. I'm - %s. Can I help you?%n", getNickname());
    }
}
