package com.sergiogetstudy.homework8;

import com.sergiogetstudy.homework8.controller.FamilyController;
import com.sergiogetstudy.homework8.dao.CollectionFamilyDao;
import com.sergiogetstudy.homework8.dao.Dao;
import com.sergiogetstudy.homework8.domain.family.Family;
import com.sergiogetstudy.homework8.domain.human.Human;
import com.sergiogetstudy.homework8.domain.human.man.Man;
import com.sergiogetstudy.homework8.domain.human.woman.Woman;
import com.sergiogetstudy.homework8.domain.pet.Pet;
import com.sergiogetstudy.homework8.domain.pet.dog.Dog;
import com.sergiogetstudy.homework8.domain.pet.domestic_cat.DomesticCat;
import com.sergiogetstudy.homework8.domain.enums.DayOfWeek;
import com.sergiogetstudy.homework8.service.FamilyService;

import java.util.*;

public class HappyFamily {
    static {
        System.out.printf("%s class is loading...%n", HappyFamily.class.getSimpleName());
    }
    public static void main(String[] args) {
        Dao<Family> collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController();

        familyService.setFamilyDao(collectionFamilyDao);
        familyController.setFamilyService(familyService);

        Human charlie = new Man("Charlie", "Runkle", (short) 1983);
        Human marcy = new Woman("Marcy", "Runkle", (short) 1984);

        familyController.createNewFamily(marcy, charlie);
        familyController.getFamilyById(0);
        familyController.addPet(0, new DomesticCat());

        Human andrew = new Man("Andrew", "Van der Beek", (short) 1953);
        Human jane = new Woman("Jane", "Van der Beek", (short) 1957);
        new Family(jane, andrew);
        familyController.createNewFamily(jane, andrew);


        Human al = new Man("Al", "Moody", (short) 1950);
        Human margaret = new Woman("Margaret", "Moody", (short) 1951);
        Family moodiesOlder = familyController.createNewFamily(jane, andrew);


        Dog catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        Set<Pet> moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        Human karen = new Woman("Karen", "Van der Beek", (short) 1981);
        Human hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                                createSchedule(), moodiesOlder, moodiesPets);

        familyController.createNewFamily(karen, hank);
        familyController.addPet(2, catStevens);

        familyController.bornChild(familyController.getFamilyById(2), "Becca", "Levon");

        Human levon = new Man("Levon", "Moody", (short) 1999);
        familyController.adoptChild(familyController.getFamilyById(2), levon);

        familyController.displayAllFamilies();

        System.out.println("familyController.deleteAllChildrenOlderThen(1) = " +
                familyController.deleteAllChildrenOlderThen(1));

        familyController.displayAllFamilies();

        System.out.println("familyController.getAllFamilies() = " +
                familyController.getAllFamilies());

        System.out.println("familyController.getPets(2) = " +
                familyController.getPets(2));

        System.out.println("familyController.getFamiliesBiggerThan(2).size() = " +
                familyController.getFamiliesBiggerThan(2).size());

        System.out.println("familyController.getFamiliesLessThan(2).size() = " +
                familyController.getFamiliesLessThan(2).size());

        System.out.println("familyController.countFamiliesWithMemberNumber() = " +
                familyController.countFamiliesWithMemberNumber(2));

        System.out.println("familyController.deleteFamilyByIndex(2) = " +
                familyController.deleteFamilyByIndex(2));

        familyController.displayAllFamilies();

        System.out.println("familyController.count() = " +
                familyController.count());


//        for (int i = 0; i < 10_000_000; i++) {
//            new Human("Agent", "Smith", (short) 1111 );
//        }
    }
    public static Map<String, String> createSchedule() {
        Map<String, String> schedule = new HashMap<>();

        for (DayOfWeek day: DayOfWeek.values()) {
            schedule.put(day.toString(), "");
        }

        return schedule;
    }
}
