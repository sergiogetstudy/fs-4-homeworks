package com.sergiogetstudy.homework10.domain.human;

import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.pet.Pet;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public abstract class Human {
    private String name;
    private String surname;
    private long birthDate;
    private byte iq;
    private Map<String, String> schedule;
    private Family family;
    private Set<Pet> pets;

    static {
        System.out.printf("%s class is loading...%n", Human.class.getSimpleName());
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, String birthDate, byte iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("d/MM/yyyy")).toEpochDay();
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate, byte iq,
                 Map<String, String> schedule, Family family, Set<Pet> pets) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
        this.pets = pets;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public byte getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    public Set<Pet> getPets() {
        return pets;
    }
    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }
    public void greetPet(Pet pet) {
        if(getPets().contains(pet)) System.out.println("Hello, " + pet.getNickname());
    }

    public boolean feedPet(boolean isTime, Pet pet) {
        if (!getPets().contains(pet)) return false;
        if(isTime) {
            System.out.printf("Hmm... I'll feed %s.%n", pet.getNickname());
            return true;
        }
        System.out.printf("I don't think %s is hungry.%n", pet.getNickname());
        return false;
    }

    public String describeAge() {
        Period period = Period.between(LocalDate.ofEpochDay(birthDate), LocalDate.now());
        return  "Years: " + period.getYears() +
                " months: " + period.getMonths() +
                " days: "+period.getDays();
    }

    public static long getStamp(String date, String format) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format)).toEpochDay();
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + LocalDate.ofEpochDay(getBirthDate()).format(DateTimeFormatter.ofPattern("d/MM/yyyy")) +
                ", iq=" + getIq() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return getBirthDate() == human.getBirthDate() &&
                getName().equals(human.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getBirthDate());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize - " + this);
    }
}
