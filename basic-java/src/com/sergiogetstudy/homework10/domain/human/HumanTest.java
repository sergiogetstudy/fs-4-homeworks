package com.sergiogetstudy.homework10.domain.human;

import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.human.man.Man;
import com.sergiogetstudy.homework10.domain.pet.Pet;
import com.sergiogetstudy.homework10.domain.pet.dog.Dog;
import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class HumanTest {
    Human simpleHuman;
    Human humanAndParents;
    Human humanAllDataInit;
    Human humanAllDataVarName;
    Human humanAllDataVarSurname;
    Human humanAllDataVarYear;
    Human humanAllDataVarIq;
    Human humanAllDataVarSchedule;
    Human humanAllDataVarFamily;
    Human humanAllDataVarPet;
    Human humanAllDataVarMother;
    Human humanAllDataVarFather;
    Human nullHuman;
    @BeforeAll
    static void beforeAll() { System.out.println("Testing Human class..."); }

    @BeforeEach
    void setUp() {
        Map<String, String> testMap = new HashMap<>();
        testMap.put("1", "1");

        Set<Pet> testPets = new HashSet<>();
        testPets.add(new Dog());

        nullHuman = new Man(null, null, 0);
        simpleHuman = new Man("", "", 0);
        humanAndParents = new Man("", "", 0);
        humanAllDataInit = new Man("", "", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarName = new Man("1", "", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarSurname = new Man("", "1", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarYear = new Man("", "", 1, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarIq = new Man("", "", 0, (byte) 1, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarSchedule = new Man("", "", 0, (byte) 0, testMap,
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarFamily = new Man("", "", 0, (byte) 0, new HashMap<>(),
                new Family(humanAllDataVarSurname, humanAllDataVarSurname), testPets);
        humanAllDataVarPet = new Man("", "", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), new HashSet<>());
        humanAllDataVarMother = new Man("", "", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
        humanAllDataVarFather = new Man("", "", 0, (byte) 0, new HashMap<>(),
                new Family(simpleHuman, simpleHuman), testPets);
    }

    @Test
    void testToString() {
        assertEquals(simpleHuman.toString(),
                "Human{name='', surname='', year=1/01/1970, iq=0}");

        assertEquals(humanAndParents.toString(),
                "Human{name='', surname='', year=1/01/1970, iq=0}");

        assertEquals(humanAllDataInit.toString(),
                "Human{name='', surname='', year=1/01/1970, iq=0}");
    }

    @Test
    void testEquals() {
        assertNotEquals(humanAllDataInit, humanAllDataVarName);
        assertEquals(humanAllDataInit, humanAllDataVarSurname);
        assertNotEquals(humanAllDataInit, humanAllDataVarYear);
        assertEquals(humanAllDataInit, humanAllDataVarIq);
        assertEquals(humanAllDataInit, humanAllDataVarSchedule);
        assertEquals(humanAllDataInit, humanAllDataVarFamily);
        assertEquals(humanAllDataInit, humanAllDataVarPet);
        assertEquals(humanAllDataInit, humanAllDataVarMother);
        assertEquals(humanAllDataInit, humanAllDataVarFather);
    }

    @Test
    void testHashCode() {
        assertNotEquals(humanAllDataInit.hashCode(), humanAllDataVarName.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarSurname.hashCode());
        assertNotEquals(humanAllDataInit.hashCode(), humanAllDataVarYear.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarIq.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarSchedule.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarFamily.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarPet.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarMother.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarFather.hashCode());
    }

    @AfterEach
    void tearDown() {
        simpleHuman = null;
        humanAndParents = null;
        humanAllDataInit = null;
        humanAllDataVarName = null;
        humanAllDataVarSurname = null;
        humanAllDataVarYear = null;
        humanAllDataVarIq = null;
        humanAllDataVarSchedule = null;
        humanAllDataVarFamily = null;
        humanAllDataVarPet = null;
        humanAllDataVarMother = null;
        humanAllDataVarFather = null;
    }

    @AfterAll
    static void afterAll() {
        System.out.println("Finish testing Human class!");
    }
}