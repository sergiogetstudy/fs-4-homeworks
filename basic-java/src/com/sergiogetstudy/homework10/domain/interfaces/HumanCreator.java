package com.sergiogetstudy.homework10.domain.interfaces;

import com.sergiogetstudy.homework10.domain.human.Human;

public interface HumanCreator {
    Human bornChild(String childMansName, String childWomansName);
}
