package com.sergiogetstudy.homework10.service;

import com.sergiogetstudy.homework10.dao.CollectionFamilyDao;
import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.human.Human;
import com.sergiogetstudy.homework10.domain.human.man.Man;
import com.sergiogetstudy.homework10.domain.human.woman.Woman;
import com.sergiogetstudy.homework10.domain.pet.Pet;
import com.sergiogetstudy.homework10.domain.pet.dog.Dog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    Human charlie, marcy;
    Human andrew, jane;
    Human al, margaret;
    Human karen, hank;
    Pet catStevens;
    Set<Pet> moodiesPets;
    Family runkles;
    Family vanDerBeek;
    Family moodiesOlder;
    Family moodies;

    List<Family> families;
    CollectionFamilyDao collectionFamilyDao;
    FamilyService familyService;

    @BeforeEach
    void setUp() {
        charlie = new Man("Charlie", "Runkle", (short) 1983);
        marcy = new Woman("Marcy", "Runkle", (short) 1984);
        runkles = new Family(marcy, charlie);

        andrew = new Man("Andrew", "Van der Beek", (short) 1953);
        jane = new Woman("Jane", "Van der Beek", (short) 1957);
        vanDerBeek= new Family(jane, andrew);

        al = new Man("Al", "Moody", (short) 1950);
        margaret = new Woman("Margaret", "Moody", (short) 1951);
        moodiesOlder = new Family(margaret, al);

        catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        karen = new Woman("Karen", "Van der Beek", (short) 1981);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new HashMap<>(), moodiesOlder, moodiesPets);
        moodies = new Family(karen, hank);

        families = new ArrayList<>();
        families.add(runkles);
        families.add(vanDerBeek);
        families.add(moodiesOlder);
        families.add(moodies);

        collectionFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService();
        familyService.setFamilyDao(collectionFamilyDao);
    }

    @AfterEach
    void tearDown() {
        charlie = null; marcy = null;
        andrew = null; jane = null;
        al = null; margaret = null;
        karen = null; hank = null;
        catStevens = null;
        moodiesPets = null;
        runkles = null;
        vanDerBeek = null;
        moodiesOlder = null;
        moodies = null;

        families = null;
        collectionFamilyDao = null;
        familyService = null;
    }

    @Test
    void getAllFamilies() {
        assertEquals(familyService.getAllFamilies(), new ArrayList<>());
        assertEquals(familyService.getAllFamilies(), collectionFamilyDao.getAllEntities());
        Family moodies = familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getAllFamilies(), new ArrayList<>(List.of(moodies)));
    }

    @Test
        void displayAllFamilies() {
        assertEquals(familyService.displayAllFamilies(), "");
        familyService.createNewFamily(karen, hank);
        assertEquals(
                familyService.displayAllFamilies(),
                "0 - Family{" +
                        "mother=Human{name='Karen', surname='Moody', year=5/06/1975, iq=0}, " +
                        "father=Human{name='Hank', surname='Moody', year=4/06/1975, iq=27}, " +
                        "children=[], pets=null}\n");
    }

    @Test
    void getFamiliesBiggerThan() {
        assertEquals(familyService.getFamiliesBiggerThan(0).size(), 0);
        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(familyService.getFamiliesBiggerThan(2).size(), 1);
        assertEquals(familyService.getFamiliesBiggerThan(-1).size(), 2);
    }

    @Test
    void getFamiliesLessThan() {
        assertEquals(familyService.getFamiliesLessThan(0).size(), 0);
        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(familyService.getFamiliesLessThan(4).size(), 2);
        assertEquals(familyService.getFamiliesLessThan(3).size(), 1);
        assertEquals(familyService.getFamiliesLessThan(-1).size(), 0);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        assertEquals(familyService.countFamiliesWithMemberNumber(0), 0);

        familyService.createNewFamily(marcy, charlie);
        Family moodies = familyService.createNewFamily(karen, hank);

        assertEquals(familyService.countFamiliesWithMemberNumber(2), 2);

        familyService.bornChild(moodies, "Levon", "Becca");

        assertEquals(familyService.countFamiliesWithMemberNumber(3), 1);
        assertEquals(familyService.countFamiliesWithMemberNumber(2), 1);
        assertEquals(familyService.countFamiliesWithMemberNumber(-1), 0);
    }

    @Test
    void createNewFamily() {
        assertEquals(familyService.createNewFamily(null, null), null);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodies = familyService.createNewFamily(karen, hank);
        assertEquals(moodies, new Family(karen, hank));
        assertEquals(familyService.getAllFamilies().size(), 1);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getAllFamilies().size(), 1);
        familyService.createNewFamily(marcy, charlie);
        assertEquals(familyService.getAllFamilies().size(), 2);
    }

    @Test
    void deleteFamilyByIndex() {
        assertFalse(familyService.deleteFamilyByIndex(-1));
        assertEquals(familyService.getAllFamilies().size(), 0);

        familyService.createNewFamily(karen, hank);
        Family runkles = familyService.createNewFamily(marcy, charlie);

        assertTrue(familyService.deleteFamilyByIndex(0));
        assertEquals(familyService.getAllFamilies().size(), 1);

        assertEquals(familyService.getFamilyById(0), runkles);
        assertFalse(familyService.deleteFamilyByIndex(1));
    }

    @Test
    void bornChild() {
        assertEquals(familyService.bornChild(null, null, null), null);
        Family moodies = new Family(karen, hank);
        assertEquals(moodies.countFamily(), 2);
        familyService.bornChild(moodies, "Levon", "Becca");
        assertEquals(moodies.countFamily(), 3);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.bornChild(moodiesInDao, "Levon", "Becca");
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().size(), 1);
    }

    @Test
    void adoptChild() {
        familyService.adoptChild(null, charlie);
        Family moodies = new Family(karen, hank);
        assertEquals(moodies.countFamily(), 2);
        familyService.adoptChild(moodies, charlie);
        assertEquals(moodies.countFamily(), 3);
        assertEquals(familyService.getAllFamilies().size(), 0);
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.adoptChild(moodiesInDao, charlie);
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().get(0), charlie);
    }

    @Test
    void deleteAllChildrenOlderThan() {
        assertFalse(familyService.deleteAllChildrenOlderThan(-1));
        assertFalse(familyService.deleteAllChildrenOlderThan(0));
        Family moodiesInDao = familyService.createNewFamily(karen, hank);
        familyService.adoptChild(moodiesInDao, charlie);
        assertTrue(familyService.deleteAllChildrenOlderThan(0));
        assertEquals(familyService.getAllFamilies().size(), 1);
        assertEquals(familyService.getFamilyById(0).getChildren().size(), 0);
    }

    @Test
    void count() {
        assertEquals(familyService.count(), 0);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.count(), 1);
        familyService.deleteFamilyByIndex(0);
        assertEquals(familyService.count(), 0);
    }

    @Test
    void getFamilyById() {
        assertSame(familyService.getFamilyById(0), null);
        collectionFamilyDao.saveEntity(runkles);
        assertSame(familyService.getFamilyById(0), collectionFamilyDao.getEntityByIndex(0));
    }

    @Test
    void getPets() {
        assertEquals(familyService.getPets(0), null);
        familyService.createNewFamily(karen, hank);
        assertEquals(familyService.getPets(0), null);
        familyService.addPet(0, new Dog());
        assertEquals(familyService.getPets(0), collectionFamilyDao.getEntityByIndex(0).getPets());
    }

    @Test
    void addPet() {
        assertEquals(familyService.addPet(0, null), false);
        familyService.createNewFamily(karen, hank);
        assertTrue(familyService.addPet(0, new Dog()));
        assertEquals(familyService.getPets(0).size(), 1);
    }
}