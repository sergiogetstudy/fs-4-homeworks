package com.sergiogetstudy.homework10.dao;

import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.human.Human;
import com.sergiogetstudy.homework10.domain.human.man.Man;
import com.sergiogetstudy.homework10.domain.human.woman.Woman;
import com.sergiogetstudy.homework10.domain.pet.Pet;
import com.sergiogetstudy.homework10.domain.pet.dog.Dog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CollectionFamilyDaoTest {
    Human charlie, marcy;
    Human andrew, jane;
    Human al, margaret;
    Human karen, hank;
    Pet catStevens;
    Set<Pet> moodiesPets;
    Family runkles;
    Family vanDerBeek;
    Family moodiesOlder;
    Family moodies;

    List<Family> families;
    CollectionFamilyDao collectionFamilyDaoTest;


    @BeforeEach
    void setUp() {
        charlie = new Man("Charlie", "Runkle", (short) 1983);
        marcy = new Woman("Marcy", "Runkle", (short) 1984);
        runkles = new Family(marcy, charlie);

        andrew = new Man("Andrew", "Van der Beek", (short) 1953);
        jane = new Woman("Jane", "Van der Beek", (short) 1957);
        vanDerBeek= new Family(jane, andrew);

        al = new Man("Al", "Moody", (short) 1950);
        margaret = new Woman("Margaret", "Moody", (short) 1951);
        moodiesOlder = new Family(margaret, al);

        catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        karen = new Woman("Karen", "Van der Beek", (short) 1981);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new HashMap<>(), moodiesOlder, moodiesPets);
        moodies = new Family(karen, hank);

        families = new ArrayList<>();
        families.add(runkles);
        families.add(vanDerBeek);
        families.add(moodiesOlder);
        families.add(moodies);

        collectionFamilyDaoTest = new CollectionFamilyDao();
    }

    @Test
    void testGetAllFamilies() {
        assertArrayEquals(collectionFamilyDaoTest.getAllEntities().toArray(), new ArrayList().toArray());
        collectionFamilyDaoTest.saveEntity(runkles);
        collectionFamilyDaoTest.saveEntity(vanDerBeek);
        collectionFamilyDaoTest.saveEntity(moodiesOlder);
        collectionFamilyDaoTest.saveEntity(moodies);
        assertArrayEquals(collectionFamilyDaoTest.getAllEntities().toArray(), families.toArray());
    }

    @Test
    void testGetFamilyByIndex() {
        assertNull(collectionFamilyDaoTest.getEntityByIndex(-1));
        assertNull(collectionFamilyDaoTest.getEntityByIndex(0));
        collectionFamilyDaoTest.saveEntity(runkles);
        assertNull(collectionFamilyDaoTest.getEntityByIndex(-1));
        assertNull(collectionFamilyDaoTest.getEntityByIndex(1));
        assertEquals(collectionFamilyDaoTest.getEntityByIndex(0), runkles);
    }

    @Test
    void testDeleteFamily() {
        assertFalse(collectionFamilyDaoTest.deleteEntity(-1));
        assertFalse(collectionFamilyDaoTest.deleteEntity(0));

        assertFalse(collectionFamilyDaoTest.deleteEntity(runkles));

        collectionFamilyDaoTest.saveEntity(runkles);
        collectionFamilyDaoTest.saveEntity(vanDerBeek);
        collectionFamilyDaoTest.saveEntity(moodiesOlder);
        assertFalse(collectionFamilyDaoTest.deleteEntity(3));
        assertTrue(collectionFamilyDaoTest.deleteEntity(2));
        assertEquals(collectionFamilyDaoTest.getAllEntities().size(), 2);

        assertTrue(collectionFamilyDaoTest.deleteEntity(runkles));
        assertEquals(collectionFamilyDaoTest.getAllEntities().size(), 1);
    }

    @Test
    void testSaveFamily() {
        collectionFamilyDaoTest.saveEntity(runkles);
        collectionFamilyDaoTest.saveEntity(vanDerBeek);
        collectionFamilyDaoTest.saveEntity(moodiesOlder);
        assertEquals(collectionFamilyDaoTest.getAllEntities().size(), 3);
        assertEquals(collectionFamilyDaoTest.getEntityByIndex(2), moodiesOlder);
        collectionFamilyDaoTest.saveEntity(moodiesOlder);
        assertEquals(collectionFamilyDaoTest.getAllEntities().size(), 3);
    }

    @AfterEach
    void tearDown() {
        charlie = null; marcy = null;
        andrew = null; jane = null;
        al = null; margaret = null;
        karen = null; hank = null;
        catStevens = null;
        moodiesPets = null;
        runkles = null; vanDerBeek = null; moodiesOlder = null; moodies = null;
        families = null;
    }
}