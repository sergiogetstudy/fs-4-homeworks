package com.sergiogetstudy.homework7.interfaces;

import com.sergiogetstudy.homework7.entities.human.Human;

public interface HumanCreator {
    Human bornChild();
}
