package com.sergiogetstudy.homework7.entities.family;

import com.sergiogetstudy.homework7.entities.pet.Pet;
import com.sergiogetstudy.homework7.entities.human.Human;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Family {
    {
        System.out.printf("%s instance is loading...%n", this.getClass().getSimpleName());
    }
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    static {
        System.out.printf("%s class is loading...%n", Family.class.getSimpleName());
    }
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.mother.setSurname(this.father.getSurname());
    }

    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public List<Human> getChildren() {
        return children;
    }
    public void setChildren(List<Human> children) {
        this.children = children;
    }
    public Set<Pet> getPets() {
        return pets;
    }
    public void setPets(Set<Pet> pets) {
        this.pets = pets;
        for (Human child: getChildren()) {
            child.setPets(pets);
        }
        mother.setPets(pets);
        father.setPets(pets);
    }

    public void addChild(Human child) {
        List<Human> children = new ArrayList<>(getChildren());

//        child.setMother(getMother());
//        child.setFather(getFather());
        child.setPets(getPets());
        children.add(child);

        setChildren(children);
    }
    public boolean deleteChild(int index) {
        List<Human> children = getChildren();

        if(index < 0 || index > children.size() - 1) return false;

        List<Human> resultArr = new ArrayList<>(getChildren());
        resultArr.remove(index);

        setChildren(resultArr);
        return true;
    }
    public boolean deleteChild(Human human) {
        List<Human> children = new ArrayList<>(getChildren());
        boolean isSucces = children.remove(human);
        if(isSucces) setChildren(children);

        return isSucces;
    }
    public int countFamily() {
        return 2 + getChildren().size();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + getMother() +
                ", father=" + getFather() +
                ", children=" + Arrays.toString(getChildren().toArray()) +
                ", pets=" + (getPets() == null ? null : getPets().toString()) +
                '}';
    }
    public void greetPet(Pet pet) {
        System.out.printf("Hello, %s", pet.getNickname());
    }
    public void describePet(Pet pet) {
        System.out.printf("В мене є %s, йому %s років, він %s ",
                pet.getSpecies(), pet.getAge(), pet.getTrickLevel());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize - " + this);
    }
}
