package com.sergiogetstudy.homework7.entities.human.woman;

import com.sergiogetstudy.homework7.entities.family.Family;
import com.sergiogetstudy.homework7.entities.human.man.Man;
import com.sergiogetstudy.homework7.entities.pet.dog.Dog;
import com.sergiogetstudy.homework7.entities.pet.Pet;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class WomanTest {
    Man simpleFather;
    Woman simpleMother;
    Family simpleFamily;
    Woman karen;
    Man hank;
    Family moodies;
    Set<Pet> pets;


    @BeforeEach
    void setUp() {
        simpleFather = new Man("", "", (short) 0);
        simpleMother = new Woman("", "", (short) 0);
        simpleFamily = new Family(simpleMother, simpleFather);
        pets = new HashSet<>();
        pets.add(new Dog(""));
        karen = new Woman("Karen", "Van der Beek",
                (short) 1981, (byte) 127,
                new HashMap<>(), simpleFamily, pets, simpleMother, simpleFather);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new HashMap<>(), simpleFamily, pets, simpleMother, simpleFather);
        moodies = new Family(karen, hank);
    }

    @AfterEach
    void tearDown() {
        simpleFather = null;
        simpleMother = null;
        simpleFamily = null;
        karen = null;
        hank = null;
        moodies = null;
    }


    @Test
    void bornChild() {
        assertNotNull(simpleMother.bornChild());
        assertNotNull(karen.bornChild());
    }
}