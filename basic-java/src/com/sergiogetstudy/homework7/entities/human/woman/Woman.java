package com.sergiogetstudy.homework7.entities.human.woman;

import com.sergiogetstudy.homework7.entities.family.Family;
import com.sergiogetstudy.homework7.entities.human.Human;
import com.sergiogetstudy.homework7.entities.human.man.Man;
import com.sergiogetstudy.homework7.entities.pet.Pet;
import com.sergiogetstudy.homework7.enums.BoyName;
import com.sergiogetstudy.homework7.enums.GirlName;
import com.sergiogetstudy.homework7.interfaces.HumanCreator;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public final class Woman extends Human implements HumanCreator {

    public Woman(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, short year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Woman(String name, String surname, short year, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet, Human mother, Human father) {
        super(name, surname, year, iq, schedule, family, pet, mother, father);
    }

    public void makeup() {
        System.out.println("Let's makeup!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname() + ". How are you?");
    }

    @Override
    public Human bornChild() {
        boolean isBoy = new Random().nextBoolean();

        if (isBoy) {
            String name = BoyName.values()[new Random().nextInt(BoyName.values().length - 1)].getName();

            return new Man(
                    name,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    (short) 0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets(),
                    getFamily().getMother(),
                    getFamily().getFather()
            );
        } else {
            String name = GirlName.values()[new Random().nextInt(BoyName.values().length - 1)].getName();

            return new Woman(
                    name,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    (short) 0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets(),
                    getFamily().getMother(),
                    getFamily().getFather()
            );
        }
    }
}
