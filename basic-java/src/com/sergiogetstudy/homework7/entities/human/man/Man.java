package com.sergiogetstudy.homework7.entities.human.man;

import com.sergiogetstudy.homework7.entities.human.Human;
import com.sergiogetstudy.homework7.entities.pet.Pet;
import com.sergiogetstudy.homework7.entities.family.Family;

import java.util.Map;
import java.util.Set;

public final class Man extends Human {
    public Man(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, short year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Man(String name, String surname, short year, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet, Human mother, Human father) {
        super(name, surname, year, iq, schedule, family, pet, mother, father);
    }

    public void repairCar() {
        System.out.println("Let's repair the car!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname());
    }
}
