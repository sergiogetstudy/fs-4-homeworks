package com.sergiogetstudy.homework6;

public class Dog extends Pet implements Foulable {
    final Species species = Species.DOG;;
    public Dog() {}

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, byte trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I'm - %s. I missed!%n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("It is necessary to replace traces well...");
    }
}
